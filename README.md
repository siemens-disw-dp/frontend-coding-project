# Newsroom Prototype

**Description**

In the course of the front-end developer duties, we are given static visual design comps from the Customer Experience team. These design comps are meant to be a starting point for discussions, the essence of which are often facilitated by taking static designs to clickable prototypes that can surface unforeseen constraints and necessary behavioral UI affordances.

---

## Task

Create a prototype that matches this Adobe XD Visual Design Comp as closely as possible (https://xd.adobe.com/view/004400f6-d49b-4c46-aee1-acbd366efb1e-5517/screen/fa342093-b7ac-4b2e-952b-2c6a935d944c). You will reference Vision, our design system and pattern library, in order to construct this prototype.

We are providing a starter template from which you can work (starter.html https://bitbucket.org/siemens-disw-dp/frontend-coding-project/src/main/starter.html, in this repository).

-   The starter template will bring in the header and footer and a sub navigational element.
-   The starter template will also bring in a very large amount of CSS that will be used to construct much of this layout. You can reference the CSS foundation and utilities (https://zeroheight.com/247bb2e30/v/latest/p/60a74b-getting-started/b/223000) and the Components (https://zeroheight.com/247bb2e30/v/latest/p/40e487-components) from our Vision Design/Pattern Library.
-   We've included the three icons from our SVG sprite in the starter template.

---

## Process and Deliverables

-   The deliverable is any end result that runs in a browser.
-   Feel free to use anything that will accomplish the above. You could simply use the existing HTML, or you could utilize a framework.
-   You are free to write any additional CSS to drive this prototype. It can be inline, in a separate file(s), and if the latter, a SCSS/SASS partial that is compiled to that separate file.
-   You are free to use any sort of Javascript (vanilla, jQuery, framework) that can help bring the prototype "to life."
-   Post the code to a repository that we can browse and from which we can download your code.
-   If there is a public URL we can view the end result at, please provide the URL. If there are runtime instructions (outside of double clicking an HTML file) please provide those separately or in a README file.

---

## Things to Consider

-   There's a "See More" button (or is it a link?) below each "section" (Press Releases, Customer Stories, Another Category). Please prototype in your best estimation what would happen if the end-user clicked on this "See More".
-   There's a row of Filters ("Filter by") at the top of this interface. Consider the implications of this user interface pattern.
-   While prototyping the implications of this "Filter by" behavioral interaction on page (or creating another prototype) is not required, prepared to answer some questions about the feasibility of the user interface pattern, how you would consider this to function, what improvements you would suggest in order to ask the visual UI/UX Designers in order to determine the requirements for this Feature.
-   You are free create additional HTML, CSS, layout patterns to communicate any suggestions for improvement, or to reinterpret parts of the existing layout. Be prepared to explain your decisions.

---

## Helpful Links

**Adobe XD Visual Design Comp**
https://xd.adobe.com/view/004400f6-d49b-4c46-aee1-acbd366efb1e-5517/screen/fa342093-b7ac-4b2e-952b-2c6a935d944c

**Vision**
https://zeroheight.com/247bb2e30/v/latest/p/065f03-vision

**Vision CSS Specs**
https://zeroheight.com/247bb2e30/v/latest/p/60a74b-getting-started/b/223000

**Vision Components**
https://zeroheight.com/247bb2e30/v/latest/p/40e487-components

**Specific Patterns/Components (that you will find helpful)**

-   Grid: https://zeroheight.com/247bb2e30/v/latest/p/889b83-layout/b/786f73
-   Spacing: https://zeroheight.com/247bb2e30/v/latest/p/889b83-layout/b/11f521
-   Flex Utilities: https://zeroheight.com/247bb2e30/v/latest/p/619f40-flex/b/0393a0
-   Promos: https://zeroheight.com/247bb2e30/v/latest/p/266b3d-promos/b/936e0e
-   Focus Line: https://zeroheight.com/247bb2e30/v/latest/p/164077-focus-line/b/49be99
-   Buttons: https://zeroheight.com/247bb2e30/v/latest/p/32408d-buttons/b/44b6ff
-   Form Selects: https://zeroheight.com/247bb2e30/v/latest/p/4252df-selection-control/b/38b0ae
-   Icon Usage: https://zeroheight.com/247bb2e30/v/latest/p/86e610-svg-sprite/b/994ca9
-   Aspect Ratio: https://zeroheight.com/247bb2e30/p/3390b7-aspect-ratios/b/856a8b

**Starter Basic Templates**
https://zeroheight.com/247bb2e30/v/latest/p/60a74b-getting-started/b/88b8b4
